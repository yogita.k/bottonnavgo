import 'package:bnbgo/bnb.dart';
import 'package:bnbgo/constant.dart';
import 'package:bnbgo/navigation_cubit.dart';
import 'package:bnbgo/pages/home.dart';
import 'package:bnbgo/pages/like.dart';
import 'package:bnbgo/pages/search.dart';
import 'package:bnbgo/pages/setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

final _rootNavigatorKey = GlobalKey<NavigatorState>();
final _shellNavigatorKey = GlobalKey<NavigatorState>();

final goRouter = GoRouter(
  initialLocation: Routerrr.homePage,
  debugLogDiagnostics: true,
  navigatorKey: _rootNavigatorKey,
  routes: [
    ShellRoute(
      navigatorKey: _shellNavigatorKey,
      builder: (context, state, child) {
        return BlocProvider(
          create: (context) => NavigationCubit(),
          child: MainScreen(child),
        );
      },
      routes: [
        GoRoute(
          path: Routerrr.homePage,
          pageBuilder: (context, state) =>
          const NoTransitionPage(
            child: Home(),
          ),
        ),
        GoRoute(
          path: Routerrr.searchPage,
          pageBuilder: (context, state) =>
          const NoTransitionPage(
            child: Search(),
          ),
        ),
        GoRoute(
          path: Routerrr.likePage,
          pageBuilder: (context, state) =>
          const NoTransitionPage(
            child: Like(),
          ),
        ),
        GoRoute(
          path: Routerrr.settingPage,
          pageBuilder: (context, state) =>
          const NoTransitionPage(
            child: Setting(),
          ),
        ),
      ],
    ),
  ],
);