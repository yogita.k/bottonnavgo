import 'package:flutter/material.dart';

class NamedNavigationItemWidget extends BottomNavigationBarItem {
  String initialLocation;

  NamedNavigationItemWidget(
      {required Widget icon, String? label, required this.initialLocation})
      : super(icon: icon, label: label);
}
