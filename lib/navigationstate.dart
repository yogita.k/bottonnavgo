import 'package:equatable/equatable.dart';

class NavigationState extends Equatable {
  final String NavigationItem;

  NavigationState(
    this.NavigationItem,
    this.index,
  );

  final int index;

  @override
  // TODO: implement props
  List<Object?> get props => [NavigationItem, index];
}
