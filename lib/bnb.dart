import 'package:bnbgo/constant.dart';
import 'package:bnbgo/navigation_cubit.dart';
import 'package:bnbgo/navigationstate.dart';
import 'package:bnbgo/named_location_navi_tem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:go_router/go_router.dart';

class MainScreen extends StatelessWidget {
  final Widget screen;

  MainScreen(this.screen);

  final tabs = [
    NamedNavigationItemWidget(
        icon: Icon(Icons.home),
        initialLocation: Routerrr.homePage,
        label: "home"),
    NamedNavigationItemWidget(
        icon: Icon(Icons.search),
        initialLocation: Routerrr.searchPage,
        label: "search"),
    NamedNavigationItemWidget(
        icon: FaIcon(FontAwesomeIcons.heart),
        initialLocation: Routerrr.likePage,
        label: "like"),
    NamedNavigationItemWidget(
        label: "settings",
        icon: Icon(Icons.settings),
        initialLocation: Routerrr.settingPage)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screen,
      bottomNavigationBar: _buildBottomNavigation(context, tabs),
    );
  }
}

BlocBuilder<NavigationCubit, NavigationState> _buildBottomNavigation(
        mContext, List<NamedNavigationItemWidget> tabs) =>
    BlocBuilder<NavigationCubit, NavigationState>(
      buildWhen: (previous, current) => previous.index != current.index,
      builder: (context, state) {
        return BottomNavigationBar(
          onTap: (value) {
            if (state.index != value) {
              context.read<NavigationCubit>().getNavBarItem(value);
              context.go(tabs[value].initialLocation);
            }
          },
          showSelectedLabels: false,
          showUnselectedLabels: false,
          elevation: 0,
          backgroundColor: Colors.black,
          unselectedItemColor: Colors.white,
          selectedIconTheme: IconThemeData(
            size: ((IconTheme.of(mContext).size)! * 1.3),
          ),
          items: tabs,
          currentIndex: state.index,
          type: BottomNavigationBarType.fixed,
        );
      },
    );
