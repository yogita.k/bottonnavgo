import 'package:bnbgo/constant.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'navigationstate.dart';

class NavigationCubit extends Cubit<NavigationState> {
  NavigationCubit() : super(NavigationState(Routerrr.homePage, 0));

  void getNavBarItem(int index) {
    switch (index) {
      case 0:
        emit(NavigationState(Routerrr.homePage, 0));
        break;
      case 1:
        emit(NavigationState(Routerrr.searchPage, 1));
        break;
      case 2:
        emit(NavigationState(Routerrr.likePage, 2));
        break;
      case 3:
        emit(NavigationState(Routerrr.settingPage, 3));
        break;
    }
  }
}
